FROM nginx:alpine
COPY ./public/* /usr/share/nginx/html/
EXPOSE 80/tcp
EXPOSE 80/udp